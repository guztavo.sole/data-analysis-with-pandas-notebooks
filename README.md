#### *These notebooks have been made following the great Udemy Course ["Data Analysis with Pandas and Python"](https://www.udemy.com/course/data-analysis-with-pandas/) by [Boris Paskhaver](https://www.udemy.com/user/borispaskhaver/)*

#### *Some definitions and ideas are also taken from the book ["Python Data Science Handbook"](http://shop.oreilly.com/product/0636920034919.do) by [Jake VanderPlas](https://www.oreilly.com/pub/au/6198)*

***
<br/><br/>
## How to use this Notebooks
- The notebooks have been developed using Pandas version 0.24.2.
- The method is to get the concepts through code examples. Comments and Notes has been reduced to the minimum necessary.
- Each Section involve one Jupyter Notebook.
- In the beginning of each Section there are all the required imports to execute the whole notebook
- Each Section is structured in Blocks that in general correspond to the lectures of the Boris Paskhaver course
- Each Block starts reading the dataset used for the examples of that Block. Despite this might drive to redundant readings, the purpose is to put things easy to the reader that jumps through blocks.
- Each Block has a Notes part at the end with a brief abstract, most relevant issues and eventually some tips concerning the content of the Block.
- The Notebooks are written thinking that you have the setup ready to get multiple Outputs in each cell. Point 2 of the following article explains how to configure this feature: https://www.dataquest.io/blog/jupyter-notebook-tips-tricks-shortcuts/

## Sections
- ### [Section 1. Introduction and Content](Section01.Introduction-and-Content.ipynb)
***
- ### [Section 2. Series](Section02.Series.ipynb)

    - Create Series objects from list or dicts
    - Attributes and Methods
    - Parameters and Arguments
    - Importing data by .read_csv() method
    - .head(), .tail(), sort_values(), sort_index()
    - Inplace Parameter
    - Extract Series values by index position or labels. The .get() method
    - .idxmax() and .idxmin() methods
    - The .value_counts() method
    - .apply() and .map() methods
***
- ### [Section 3. DataFrames I](Section03.DataFrames-I.ipynb)
    - Shared Methods with Series
    - Selecting and adding Columns
    - Broadcasting
    - Managing nulls. .dropna() and .fillna() methods
    - .astype() method
    - Sorting and Rank
***
- ### [Section 4. DataFrames II](Section04.DataFrames-II.ipynb)
    - Filtering with one or more conditions.
    - Filtering with boolean Series created with .isin(), .between(), .isnull(), .notnull() methods
    - Managing duplicates with .duplicated(), .drop_duplicates(), .unique() and .nunique() methods
***
- ### [Section 5. DataFrames III](Section05.DataFrames-III.ipynb)
    - .set_index() and .reset_index()
    - Retrieving Rows with .loc[] and .iloc[]. The second argument of .loc[] and .iloc[]. Indexers .at[] and .iat[]
    - Setting new values for a specific Cell. Setting multiple values at once
    - Rename index Labels and Columns
    - Deleting Rows or Columns
    - Creating Random samples with .sample() method.
    - .nsmallest() and nlargest() methods
    - Filtering with the .where() method
    - The .query() method
    - The .apply() method with Row values
    - The .copy() method
***
- ### [Section 6. Working with Text data](Section06.Working-with-Text-Data.ipynb)
    - The .str. prefix. Common string methods: .str.lower(), .str.upper(), .str.title(), .str.len()
    - The str.replace() method
    - Filtering with String Boolean Methods. .str.contains(), .str.startswith(), .str.endswith()
    - .str.strip(), .str.lstrip() and .str.rstrip()
    - Using String Methods on Index Labels and Column names
    - Splitting Strings by Characters with .str.strip() method
***
- ### [Section 7. MultiIndex](Section07.MultiIndex.ipynb)
    - Creating a MultiIndex with the .set_index() method
    - The .get_level_values() method
    - The .set_names() method
    - The .sort_index() method
    - Extract Rows from a MultiIndex DataFrame
    - The .transpose() method. MultiIndex on Columns
    - The .swaplevel() method
    - .stack() and .unstack() methods
    - .pivot(), .pivot_table() and .melt() methods
***
- ### [Section 8. GroupBy](Section08.GroupBy.ipynb)
    - The .groupby() method and the GroupBy object
    - The .get_group() method
    - Methods on a GroupBy object: min, max, mean, sum,...
    - Grouping by multiple columns
    - The .agg() method
    - Iterating through groups
    - .aggregate(), .filter(), .transform(), and .apply() Methods
    - .pivot_table() as a two-dimensional GroupBy operation
***
- ### [Section 9. Merging, Joining and Concatenating](Section09.Merging-Joining-and-Concatenating.ipynb)
    - The .concat() Method. The .append() Method as a particular case of .concat()
    - The .merge() Method with inner, outer and left Joins.
    - The left_on and right_on parameters
    - Merging by indexes with the left_index and right_index parameters
    - The .join() Method as a particular case of .merge()
***
- ### [Section 10. Working with Dates and Times](Section10.Working-with-Dates-and-Times.ipynb)
    - Review of Python datetime module
    - The pandas Timestamp object
    - The pandas DatetimeIndex object
    - The pd.to_datetime() and the pd.date_range() methods
    - The .dt. accessor
    - Selecting Rows with the DatetimeIndex
    - The .truncate() method
    - The pandas DateOffset object
    - The pandas Timedelta object
***
- ### [Section 11. Panels (Deprecated)](Section11.Panels(Deprecated).ipynb)
***
- ### [Section 12. Input and Output](Section12.Input-and-Output.ipynb)
    - The .read_csv() method. Using urls
    - Export a DataFrame to csv with .to_csv() method
    - Importing and exporting Excel files
***
- ### [Section 13. Visualization](Section13.Visualization.ipynb)
    - The .plot() method
    - Configuration of styles
    - Bar Graphs, Pie Charts and Histograms
***
- ### [Section 14. Options and Settings](Section14.Options-and-Settings.ipynb)
    - Changing pandas Options with Attributes Dot Syntax
    - Changing pandas Options with Methods. .get_option(), .set_option(), .reset_option() and .describe_option()
    - The precision Option